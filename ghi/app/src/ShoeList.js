import React from 'react';





function ShoeList(props) {
    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Shoe</th>
                        <th>Color</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map((shoe) => {
                        return (
                            <tr key={shoe.id}>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.picture_url}</td>
                                <td>{shoe.bin}</td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => this.delete(shoe.id)} >Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ShoeList;
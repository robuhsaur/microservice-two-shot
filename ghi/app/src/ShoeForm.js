import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            modelName: '',
            color: '',
            manufacturer: '',
            pictureUrl: '',
            bin: "",
            bins: []
        };
        this.handleModelNameChange = this.handleModelNameChange.bind(this); //
        this.handleColorChange = this.handleColorChange.bind(this); //
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this); //
        this.handlePictureURLChange = this.handlePictureURLChange.bind(this); //
        this.handleBinChange = this.handleBinChange.bind(this); // 
        this.handleSubmit = this.handleSubmit.bind(this); //
    }

    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState({ modelName: value })
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }
    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value })
    }
    handlePictureURLChange(event) {
        const value = event.target.value;
        this.setState({ pictureUrl: value })
    }
    handleBinChange(event) {
        const value = event.target.value;
        this.setState({ bin: value })
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.picture_url = data.pictureUrl;
        data.model_name = data.modelName;
        delete data.pictureUrl;
        delete data.bins;
        delete data.modelName

        const binUrl = 'http://localhost:8080/api/shoes/';   //where we are posting to 
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {
            const newBin = await response.json();

            const cleared = {
                modelName: '',
                color: '',
                manufacturer: '',
                pictureUrl: '',
                bin: '',
            };
            this.setState(cleared);
        }
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';  //grabbing info from 

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins });
            // const selectTag = document.getElementById('state');
            // for (let state of data.states) {
            //     const option = document.createElement('option');
            //     option.value = state.abbreviation;
            //     option.innerHTML = state.name;
            //     selectTag.appendChild(option);
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new shoe</h1>
                        <search onSubmit={this.handleSubmit} id="create-shoe-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleModelNameChange} value={this.state.modelName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="modelName">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} value={this.state.color} placeholder="color" required type="text" name="fabric" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                                <label htmlFor="manufacturer">manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureURLChange} value={this.state.pictureUrl} placeholder="pictureurl" required type="text" name="pictureurl" id="pictureurl" className="form-control" />
                                <label htmlFor="pictureurl">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleBinChange} value={this.state.bin} required name="bin" id="bin" className="form-select">
                                    <option value="">Choose a bin</option>
                                    {this.state.bins.map((bin) => {
                                        return (
                                            <option key={bin.id} value={bin.id}>
                                                {bin.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </search>
                    </div>
                </div>
            </div>
        );
    }
}
export default ShoeForm;